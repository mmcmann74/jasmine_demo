(function (root, common, $, ko) {

    "use strict";

    var ModuleViewModel = (function () {

        // Constructor ------------------------
        function ModuleViewModel(params) {
            var self = this;
            self.firstName = ko.observable("Mike");
            self.lastName = ko.observable("McMann");
            self.fullName = ko.pureComputed(function() {
                return self.firstName() + " " + self.lastName();
            });
        }

        ModuleViewModel.version = "1.2.0";

        return ModuleViewModel;

    })();

    root.ModuleViewModel = ModuleViewModel; // default export

})((Safelite.createNamespace("ModuleName")), Safelite.Common, $, ko);