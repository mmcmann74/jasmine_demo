(function (root, ko, defaults) {

    "use strict";

    root.init = function () {

        root.koViewModel = new Safelite.ModuleName.ModuleViewModel(defaults);
        ko.applyBindings(root.koViewModel);

    };

})(Safelite.createNamespace("PageModule"), ko, { default: "value" });