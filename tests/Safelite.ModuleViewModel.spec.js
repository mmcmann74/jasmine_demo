(function (context) {

    var params = {},
        sutObj;

    beforeEach(function () {
        sutObj = new Safelite.ModuleName.ModuleViewModel(params);
    });

    afterEach(function () {
        sutObj = null;
    });

    describe("This ViewModel", function () {

        it("should have a first name", function () {
            expect(sutObj.firstName()).toBe("Mike");
        });

        it("should have a full name", function () {
            expect(sutObj.fullName()).toBe("Mike McMann");
        });
    });

})(this);