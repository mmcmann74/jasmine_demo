# Frontend Unit Tests with Jasmine and Karma #

* Make sure Node and npm are installed.
* [Jasmine Docs](https://jasmine.github.io/2.0/introduction.html)

## Step 0: Init ##

Open step-0:

`git checkout -f step-0`

`npm install`

## Step 1: Jasmine -- first steps ##

Open step-1:

`git checkout -f step-1`

If not already installed, install lite-server.

`npm i lite-server -g`

Create the `bs-config.json` file.

cd /path/to/testproject

`lite-server`

Make sure you have your local digital project running.

Open the browser and type in _http://localhost:3000/SpecRunner.html_.

Unit test:

```javascript
// Inline spec...
(function (context) {

    describe("This Jasmine deal", function () {

        it("should know the truth", function () {
            expect(true).toBe(false);
        });

    });

}).call(this);
```

Change false to true. Works like a champ!!

## Step 2: Karma ##

Open step-2:

`git checkout -f step-2`

`npm install`

Review Gruntfile.js

Review karma.config.js

`karma start`

To see `tests/truth.spec.js` in the browser runner:

`lite-server`

Open the browser and type in _http://localhost:3000/SpecRunner.html_.

## Step 3: Making stuff testable ##

### No good ###

`Safelite.PageModule.js`

```javascript
(function (module, common, $, ko) {

    module.init = function () {

        var koViewModel = new ModuleViewModel(common, ko, module.viewModel);
        ko.applyBindings(koViewModel);

    };

})(Safelite.createNamespace("PageModule"), Safelite.Common, $, ko);

function ModuleViewModel(common, ko, pageVm) {
    var self = this;
}

ModuleViewModel.version = "1.2.0";
```

### Better ###

`Safelite.PageModule.js`

```javascript
(function (root, common, $, ko, defaults) {

    "use strict";

    root.init = function () {
        var mergedParams = $.extend(true, defaults || {}, root.viewModel || {});
        root.koViewModel = new ModuleViewModel(mergedParams);
        ko.applyBindings(root.koViewModel);
    };

    var ModuleViewModel = (function () {

        // Constructor ------------------------
        function ModuleViewModel(params) {
            var self = this;
        }

        ModuleViewModel.version = "1.2.0";

        return ModuleViewModel;

    })();

    root.ModuleViewModel = ModuleViewModel; // default export

})(Safelite.createNamespace("PageModule"), Safelite.Common, $, ko, { default: "value" });
```

Use the "data-last" argument pattern to prepare for the future, Conan: functional JavaScript.

### Best ###

Decouple, make module AMD/CommonJS compatible-ish. Useful for the future: migration to ES6/TypeScript/etc.

[Making Your Library Support AMD and CommonJS](http://ifandelse.com/its-not-hard-making-your-library-support-amd-and-commonjs/)

```javascript
// files/Safelite.PageModule.js
(function (root, ko, defaults) {

    "use strict";

    root.init = function () {

        root.koViewModel = new Safelite.ModuleName.ModuleViewModel(defaults);
        ko.applyBindings(root.koViewModel);

    };

})(Safelite.createNamespace("PageModule"), ko, { default: "value" });

// files/Safelite.ModuleViewModel.js
(function (root, common, $, ko) {

    "use strict";

    var ModuleViewModel = (function () {

        // Constructor ------------------------
        function ModuleViewModel(params) {
            var self = this;
        }

        ModuleViewModel.version = "1.2.0";

        return ModuleViewModel;

    })();

    root.ModuleViewModel = ModuleViewModel; // default export

})(Safelite.createNamespace("ModuleName"), Safelite.Common, $, ko);
```

- Best because you can include only the view model file and instantiate it in the test,
not in the page module init.
- Use "root" because "module" is a reserved word with CommonJS.

## Step 4: Write a test ##

files/Safelite.ModuleViewModel.js

tests/Safelite.ModuleViewModel.spec.js