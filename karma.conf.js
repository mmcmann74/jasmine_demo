// Karma configuration
// Generated on Wed Apr 06 2016 15:57:35 GMT-0400 (Eastern Daylight Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    // set these in the Gruntfile. See meta.lib.files
    files: [
      // include lib files here...
      "http://localhost:38000/Shared/js/lib/knockout-3.2.0.js",
      "http://localhost:38000/Shared/js/lib/knockout.mapping-latest.js",
      "http://localhost:38000/Shared/js/lib/knockout-postbox/build/knockout-postbox.js",
      "http://localhost:38000/Shared/js/lib/lodash.compat.js",
      "http://localhost:38000/Shared/js/lib/jquery-2.1.0.min.js",
      "http://localhost:38000/Shared/js/lib/jquery.validate.js",
      "http://localhost:38000/Shared/js/lib/bootstrap.js",
      "http://localhost:38000/Shared/js/Safelite.js",
      "http://localhost:38000/Shared/js/Safelite.Common.js",
      "http://localhost:38000/Shared/js/Safelite.CustomerPortal.Enums.js",
      // include source files here...
      "./files/**/*.js",
      "./tests/**/*.spec.js",
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'], //['Chrome', 'PhantomJS', 'IE'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
